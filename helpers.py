
from models import Item as ItemModel
async def get_items(items):
    query  = [{'$lookup': 
                {'from' : 'category',
                 'localField' : 'category',
                 'foreignField' : 'name',
                 'as' : 'category_d'},
                 },
            {"$unwind":"$category_d" },
            {'$lookup': 
                {'from' : 'subcategory',
                 'localField' : 'subcategory',
                 'foreignField' : 'name',
                 'as' : 'subcategory_d'},
            },
            {"$unwind":"$subcategory_d" },
            { 
                '$match' : { 'name' : {'$in':items}}
            },
            {
                "$project":{
                "name":1,
                "subcategory":1,
                "category":1,
                "discount_type":1,
                "discount":1,
                "discount_type":1,
                "category_discount":"$category_d.discount",
                "subcategory_discount": "$subcategory_d.discount",

                "quantity_unit": 1,
                "rate": 1

                
            }}
            ]
    data_items = ItemModel.connection().aggregate(query)
    data = [i async for i in data_items]
    return data


def discount_helper(item, quantity):
    total_payable = item['rate'] * quantity
    if item["discount_type"] == "bg":
        buy = item['discount']['buy']
        get = item['discount']['get']
        payable = (quantity // (buy+get))* buy
        payable += quantity % (buy+get)
        actual_payable = item['rate'] * payable
        return total_payable, actual_payable

    else:
        max_discount = max(item.get('category_discount',0), item.get('subcategory_discount',0), item.get('discount',0).get('flat',0))
        discount = (total_payable * max_discount)//100 
        actual_payable = total_payable - discount
        return total_payable, actual_payable