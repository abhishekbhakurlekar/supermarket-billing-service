

class NormalizeErrors:
    _ignore_list = ('slug', 'uid', 'id', 'tag', 'field')

    def __init__(self, error_type: str, errors: dict):
        self.error_type = error_type
        self.errors = errors
        self.processed_errors = []

    @staticmethod
    def _normalize_string(string):
        string = str(string)
        return string.capitalize().strip()

    def serializer_error(self, errors: dict, base_key: str = None) -> str:
        for key, val in errors.items():
            key = NormalizeErrors._normalize_string(key)
            key = ' '.join([i for i in key.split(
                '_') if i and i not in self._ignore_list])

            if isinstance(val, list):
                key = NormalizeErrors._normalize_string(key)
                record = f"{key}: {val[0]}"
                if base_key:
                    record = f"{NormalizeErrors._normalize_string(base_key)} {record}"
                
                record = record.replace('Schema:', '').strip()
                self.processed_errors.append(record)
            elif isinstance(val, dict):
                self.serializer_error(
                    val, base_key=None if key.isdigit() else key)

        return ', '.join(self.processed_errors).replace('.', '')

    def normalize(self) -> str:
        if self.error_type in ('marshmallow', 'cerberus'):
            return self.serializer_error(self.errors)
