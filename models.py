from databases import client
from schemas import SubCategorySchema, CategorySchema, ItemSchema, UserSchema
from billing.validator import InvoiceSchema
class Database_models:
    db = client['supermarket']
    @classmethod
    def connection(cls):
        return cls.db[cls.collection]

class Category(Database_models) :
    insertSchema = CategorySchema
    collection = "category"


class SubCategory(Database_models) :
    insertSchema = SubCategorySchema
    collection = "subcategory"
    

class Item(Database_models) :
    insertSchema = ItemSchema
    collection = "item"

class User(Database_models):
    insertSchema = UserSchema
    collection = 'user'

class Invoice(Database_models):
    insertSchema = InvoiceSchema
    collection = 'invoice'