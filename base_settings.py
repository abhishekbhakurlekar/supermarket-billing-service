
from pydantic import BaseSettings


class Settings(BaseSettings):
    """
    Configuration for the application.
    """
    JWT_SECRET_KEY: str
    ALGORITHM: str
    KAFKA_PORT : str
    COMPANY_MAIL_ID : str
    COMPANY_MAIL_PASS : str
    class Config:
        env_file = ".env"

settings = Settings()
