from marshmallow import ValidationError
from models import SubCategory as SubCategoryModel, Category as CategoryModel
from fastapi import Request, HTTPException, status, APIRouter,Response
from pymongo.errors import DuplicateKeyError


subcategory_router = APIRouter(prefix="/subcategory", tags=['subcategory'])



@subcategory_router.get('/{sub_category_name}')
async def get_subcategory(sub_category_name: str):
    
    subcategory = await SubCategoryModel.connection().find_one({"name":sub_category_name.lower()})
    if not subcategory:
        raise HTTPException(status_code= status.HTTP_400_BAD_REQUEST, detail= f"sorry this subcategory {sub_category_name} not found")
    
    resp_dict = SubCategoryModel.insertSchema().dump(subcategory)
    
        
    return resp_dict
    

@subcategory_router.put('/{sub_category_name}', status_code=status.HTTP_202_ACCEPTED)
async def put_subcategory(request: Request ,sub_category_name: str):
    data = await request.json()
    
    subcategory = await SubCategoryModel.connection().find_one({"name":sub_category_name.lower()})
    if not subcategory:
        raise HTTPException(status_code= status.HTTP_400_BAD_REQUEST, detail= f"sorry this subcategory {sub_category_name} not found")
    
    try:
        serialized_data = SubCategoryModel.insertSchema().load(data)
        category = await CategoryModel.connection().find_one({"name":serialized_data['category'].lower()})
        if not category:
            raise HTTPException(status_code= status.HTTP_400_BAD_REQUEST, detail= f"sorry this category {serialized_data['category'].lower()} not found")
        
    except ValidationError as err:
        raise HTTPException(status_code= status.HTTP_400_BAD_REQUEST, detail= err.messages)
    
    await SubCategoryModel.connection().update_one({"name":sub_category_name.lower()},{"$set": serialized_data})
    
    return {"success": True}



@subcategory_router.post('/', status_code=status.HTTP_202_ACCEPTED)
async def post_subcategory(request: Request):
    data = await request.json()
    try:
        serialized_data = SubCategoryModel.insertSchema().load(data)
        category = await CategoryModel.connection().find_one({"name":serialized_data['category'].lower()})
        if not category:
            raise HTTPException(status_code= status.HTTP_400_BAD_REQUEST, detail= f"sorry this category {serialized_data['category'].lower()} not found")
        result = await SubCategoryModel.connection().insert_one(serialized_data)
        
    except ValidationError as err:
        raise HTTPException(status_code= status.HTTP_400_BAD_REQUEST, detail= err.messages)
    except DuplicateKeyError:
        raise HTTPException(status_code= status.HTTP_400_BAD_REQUEST, detail= "Discount on these subcategory already exists please try to update it")

    
    return {"success": True}
