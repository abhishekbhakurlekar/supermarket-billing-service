import Vue from 'vue'
import App from './App.vue'
import Billing from './Billing.vue'
import VueSimpleAlert from "vue-simple-alert";
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueRouter from 'vue-router'
import Routes from './routes'


Vue.use(VueSimpleAlert);
Vue.use(VueAxios, axios)
Vue.use(VueRouter)
const router = new VueRouter  ({
  routes: Routes
})

new Vue({
  el: '#app',
  render: h => h(App),
  router: router
})
