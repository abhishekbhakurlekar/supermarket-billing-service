import login from './components/login.vue'
import Billing from './Billing.vue';
import signup from './components/signup.vue'
export default[
    {path : "/login", component:login},
    {path : "/billing", component:Billing},
    {path : "/signup", component:signup}
]

