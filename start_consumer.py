from aiokafka import AIOKafkaConsumer
import asyncio
import motor.motor_asyncio
import json
import ast
from base_settings import settings
from config import KAFKA_TOPIC_DICT
from consumer_config import KAFKA_ACTIONS_DICT

def get_task(data):
    task_type = data.get('meta',{}).get('action')
    if not task_type:
        print(data)
    action = KAFKA_ACTIONS_DICT.get(task_type, {}).get("main_task")
    if not action:
        print(data)
    return action
    



async def consume():
    topics = list(KAFKA_TOPIC_DICT.values())
    consumer = AIOKafkaConsumer(*topics,
        bootstrap_servers=settings.KAFKA_PORT)
    await consumer.start()
    try:
        async for msg in consumer:
            document = ast.literal_eval(msg.value.decode('utf-8'))
            action = get_task(document)
            asyncio.create_task(action(document))
    except Exception as e:
        print(e)

    finally:
        await consumer.stop()



loop = asyncio.get_event_loop()
loop.create_task(consume())
loop.run_forever()