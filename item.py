from marshmallow import ValidationError
from models import Item as ItemModel, Category as CategoryModel, SubCategory as SubCategoryModel
from fastapi import Request, HTTPException, status, APIRouter,Response
from pymongo.errors import DuplicateKeyError


item_router = APIRouter(prefix="/item", tags=['item'])



@item_router.get('/{item_name}')
async def get_item(item_name: str):
    
    item = await ItemModel.connection().find_one({"name":item_name.lower()})
    if not item:
        raise HTTPException(status_code= status.HTTP_400_BAD_REQUEST, detail= f"sorry this item {item_name} not found")
    
    resp_dict = ItemModel.insertSchema().dump(item)
    
        
    return resp_dict
    

@item_router.put('/{item_name}', status_code=status.HTTP_200_OK)
async def put_item(request: Request ,item_name: str):
    data = await request.json()
    
    item = await ItemModel.connection().find_one({"name":item_name.lower()})
    if not item:
        raise HTTPException(status_code= status.HTTP_400_BAD_REQUEST, detail= f"sorry this item {item_name} not found")
    
    try:
        serialized_data = ItemModel.insertSchema().load(data)
        category = await CategoryModel.connection().find_one({"name":serialized_data['category'].lower()})
        if not category:
            raise HTTPException(status_code= status.HTTP_400_BAD_REQUEST, detail= f"sorry this category {serialized_data['category'].lower()} not found")
        subcategory = await SubCategoryModel.connection().find_one({"name":serialized_data['subcategory'].lower()})
        if not subcategory:
            raise HTTPException(status_code= status.HTTP_400_BAD_REQUEST, detail= f"sorry this subcategory {serialized_data['subcategory']} not found")
        
        
    except ValidationError as err:
        raise HTTPException(status_code= status.HTTP_400_BAD_REQUEST, detail= err.messages)
    
    await ItemModel.connection().update_one({"name":item_name.lower()},{"$set": serialized_data})
    
    return {"success": True}



@item_router.post('/', status_code=status.HTTP_202_ACCEPTED)
async def post_item(request: Request):
    data = await request.json()
    try:
        serialized_data = ItemModel.insertSchema().load(data)
        category = await CategoryModel.connection().find_one({"name":serialized_data['category'].lower()})
        if not category:
            raise HTTPException(status_code= status.HTTP_400_BAD_REQUEST, detail= f"sorry this category {serialized_data['category'].lower()} not found")
        subcategory = await SubCategoryModel.connection().find_one({"name":serialized_data['subcategory'].lower()})
        if not subcategory:
            raise HTTPException(status_code= status.HTTP_400_BAD_REQUEST, detail= f"sorry this subcategory {serialized_data['subcategory']} not found")
        
        result = await ItemModel.connection().insert_one(serialized_data)
        
    except ValidationError as err:
        raise HTTPException(status_code= status.HTTP_400_BAD_REQUEST, detail= err.messages)
    except DuplicateKeyError:
        raise HTTPException(status_code= status.HTTP_400_BAD_REQUEST, detail= "Discount on these item already exists please try to update it")

    
    return {"success": True}
