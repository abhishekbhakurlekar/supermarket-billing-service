import imp
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
import category
from billing.function import billing_router
import subcategory
import item
from auth.function import auth_router
app = FastAPI()

origins = ["*"
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(billing_router)


app.include_router(category.category_router)


app.include_router(subcategory.subcategory_router)

app.include_router(item.item_router)

app.include_router(auth_router)