import os
import asyncio
from base_settings import settings
from InvoiceGenerator.api import Invoice, Item, Client, Provider, Creator
from InvoiceGenerator.pdf import SimpleInvoice
from models import User as UserModel, Invoice as InvoiceModel
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders


os.environ["INVOICE_LANG"] = "en"


class InvoiceHelper:
    @classmethod
    async def send_email(cls, invoice, c_email):
        msg = MIMEMultipart()
        fromaddr = settings.COMPANY_MAIL_ID
        msg["From"] = fromaddr

        msg["To"] = c_email
        msg["Subject"] = ""
        body = "Thanks for shopping with us Come again"
        msg.attach(MIMEText(body, "plain"))

        filename = invoice
        attachment = open(f"{os.getcwd()}/{filename}", "rb")
        p = MIMEBase("application", "octet-stream")

        p.set_payload((attachment).read())
        encoders.encode_base64(p)
        p.add_header("Content-Disposition", "attachment; filename= %s" % filename)

        msg.attach(p)
        # s = smtplib.SMTP("smtp.gmail.com", 587)
        # s.starttls()
        # s.login(fromaddr, settings.COMPANY_MAIL_PASS)
        text = msg.as_string()
        # s.sendmail(fromaddr, toaddr, text) google error
        # s.quit()

    @staticmethod
    async def get_user_data(data):
        email_id = data.get("user")
        user = await UserModel.connection().find_one(
            {"email_id": email_id}, {"name": 1}
        )
        return user.get("name")

    @classmethod
    async def generate_pdf(cls, data, **kwargs):
        data['created_on'] = data.get('meta',{}).get('created_at') or  datetime.datetime.now().isoformat()
        try:
            data = InvoiceModel.insertSchema().load(data)
            _id = await InvoiceModel.connection().insert_one(data)
            
        except Exception as e:
            print(e)
            print(data)
        customer_name = data.get("customer", {}).get("name")
        user_name = await cls.get_user_data(data)
        client = Client(customer_name)

        provider = Provider(
            "Supermarket Limited", bank_account="6454-6361-217273", bank_code="2021"
        )

        creator = Creator(user_name)

        invoice = Invoice(client, provider, creator)
        items = data.get("items", {}).get("items", [])
        for it in items:
            name, quantity = it.get("item"), it.get("quantity")
            per_price = it.get("amount") / quantity

            invoice.add_item(
                Item(
                    count=quantity,
                    price=per_price,
                    description=name,
                    unit=it.get("quantity_unit", ""),
                )
            )

        invoice.currency = "Rs."
        invoice.number = 123445
        docu = SimpleInvoice(invoice)
        customer_email = data.get("customer", {}).get("email_id", "")
        name = f"{str(_id.inserted_id)}.pdf"
        docu.gen(name, generate_qr_code=False)

        return name, customer_email

    @classmethod
    async def generate_invoice(cls, data, *args, **kwargs):
        invoice_name, customer_email = await cls.generate_pdf(data)
        await cls.send_email(invoice_name, customer_email)


