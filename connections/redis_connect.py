import redis
import json
from datetime import timedelta

class RedisCache:
    def __init__(self):
        self.conn = redis.Redis(host="localhost", port="6379", db = 0, socket_timeout=5)

    def connect(self):
        return self.conn
