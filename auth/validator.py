from marshmallow import EXCLUDE, Schema, fields, post_load, pre_load
from marshmallow.validate import OneOf
from auth.helper import SignUpHelper
from schemas import LowerString

class UserSchema(Schema):
    name = LowerString(required=True)
    email_id = fields.Email(required=True)
    password = fields.Str(required=True)

    class Meta:
        unknown = EXCLUDE

    @post_load
    def post_load(self, data, **kwargs):
        if data.get('password'):
            data['password'] = SignUpHelper.bcrypt(data.get('password',""))
        return data


class UserLoginSchema(Schema):
    email_id = fields.Email(required=True)
    password = fields.Str(required=True)

    class Meta:
        unknown = EXCLUDE
