
import os
from datetime import datetime, timedelta
from typing import Any, Union

from fastapi import Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer
from jose import jwt
from passlib.context import CryptContext
from pydantic import ValidationError

from models import User
from base_settings import settings


class UserAuth:
    reuseable_oauth = OAuth2PasswordBearer(
        tokenUrl="/login",
        scheme_name="JWT"
    )

    @classmethod
    async def get_current_user(cls, token: str = Depends(reuseable_oauth)):
        try:
            payload = jwt.decode(
                token, settings.JWT_SECRET_KEY, algorithms=[settings.ALGORITHM]
            )
            if datetime.fromtimestamp(payload.get("exp")) < datetime.now():
                raise HTTPException(
                    status_code = status.HTTP_401_UNAUTHORIZED,
                    detail="Session expired Please Login",
                    headers={"WWW-Authenticate": "Bearer"},
                )
            return payload.get('sub','')
        except(jwt.JWTError, ValidationError):
            raise HTTPException(
                status_code=status.HTTP_403_FORBIDDEN,
                detail="Could not validate credentials",
                headers={"WWW-Authenticate": "Bearer"},
            )




class SignUpHelper:
    pwd_cxt = CryptContext(schemes =["bcrypt"],deprecated="auto")
    @classmethod
    def bcrypt(cls,password:str):
        return cls.pwd_cxt.hash(password)

    @classmethod
    def verify(cls,hashed,normal):

        return cls.pwd_cxt.verify(normal,hashed)

    async def create_user(self, data):
        try:
            await User.connection().insert_one(data)
        except Exception as err:
            print(err)
        return data


    @classmethod
    def create_access_token(cls, subject: Union[str, Any], expires_delta: int = None) -> str:
        if expires_delta is not None:
            expires_delta = datetime.utcnow() + expires_delta
        else:
            expires_delta = datetime.utcnow() + timedelta(minutes=300)
        
        to_encode = {"exp": expires_delta, "sub": str(subject)}
        encoded_jwt = jwt.encode(to_encode, settings.JWT_SECRET_KEY, settings.ALGORITHM)
        return encoded_jwt

    
        