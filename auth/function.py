from marshmallow import ValidationError
from fastapi import Request, HTTPException, status, APIRouter
from auth.validator import UserSchema, UserLoginSchema
from models import User as UserModel
from auth.helper import SignUpHelper

auth_router = APIRouter(prefix="/auth", tags=["login"])


@auth_router.post("/")
async def create_emp(request: Request):
    user_data = await request.json()
    try:
        data = UserSchema().load(user_data)
    except ValidationError as err:
        error_msg = NormalizeErrors('marshmallow', err.messages).normalize()
        raise HTTPException(status_code= status.HTTP_400_BAD_REQUEST, detail= error_msg)
    email_id = data.get("email_id")
    existing_user = await UserModel.connection().find_one({"email_id": email_id})
    if existing_user:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail="User already exists")

    usrhelper = SignUpHelper()
    await usrhelper.create_user(data)

    
    return {"success": True}


@auth_router.post('/login', summary="Create access and refresh tokens for user")
async def login(request: Request):
    user_data = await request.json()
    try:
        data = UserLoginSchema().load(user_data)
    except ValidationError as err:
        error_msg = NormalizeErrors('marshmallow', err.messages).normalize()
        raise HTTPException(status_code= status.HTTP_400_BAD_REQUEST, detail= error_msg)
    email_id = data.get("email_id","")
    existing_user = await UserModel.connection().find_one({"email_id": email_id})
    if existing_user is None:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Incorrect email or password"
        )

    hashed_pass, normal = existing_user.get('password',""), data.get('password',"")
    if not SignUpHelper.verify(hashed_pass, normal):
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Incorrect email or password"
        )
    
    return {
        "access_token": SignUpHelper.create_access_token(email_id)
    }