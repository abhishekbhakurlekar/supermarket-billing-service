from marshmallow import EXCLUDE, Schema, fields, post_load, pre_load
from marshmallow.validate import OneOf

QUANTITY_UNIT = ["kg", "g", "lt", "ml", "dz", "no"]


class LowerString(fields.String):
    def _deserialize(self, value, *args, **kwargs):
        if hasattr(value, "lower"):
            value = value.lower()
        return super()._deserialize(value, *args, **kwargs)

    def _serialize(self, value, *args, **kwargs):
        if hasattr(value, "lower"):
            value = value.lower()
        return super()._serialize(value, *args, **kwargs)


class GetitemSchema(Schema):
    name = LowerString(required=True)
    quantity = fields.Float(required=True)
    quantity_unit = LowerString(required=True, validate=OneOf(QUANTITY_UNIT))

    class Meta:
        unknown = EXCLUDE


class ItemSchema(Schema):
    name = LowerString(required=True)
    subcategory = LowerString(required=True)
    category = LowerString(required=True)
    discount = fields.Dict(required=True)
    rate = fields.Int(required=True)
    quantity_unit = LowerString(required=True)
    discount_type = LowerString(required=True, validate=OneOf(choices=["flat", "bg"]))

    class Meta:
        unknown = EXCLUDE


class SubCategorySchema(Schema):
    name = LowerString(required=True, validate=bool)
    category = LowerString(required=True, validate=bool)
    discount = fields.Int(required=True)

    class Meta:
        unknown = EXCLUDE


class CategorySchema(Schema):
    name = LowerString(required=True)
    discount = fields.Int(required=True)

    class Meta:
        unknown = EXCLUDE


class UserSchema(Schema):
    name = LowerString(required=True)
    email_id = fields.Email(required=True)
    password = fields.Str(required=True)




class CustomerSchema(Schema):
    name = LowerString(required=True)
    email_id = fields.Email(required=True)



class BillSchema(Schema):
    items = fields.List(fields.Nested(GetitemSchema, required= True), required=True)
    customer = fields.Nested(CustomerSchema)
    user = fields.Str(required=True)
    
    class Meta:
        unknown = EXCLUDE


class ItemBillSchema(Schema):
    item = fields.Str(required=True)
    quantity = fields.Int(required=True)
    amount = fields.Float(required=True)
    quantity_unit = LowerString(required=True)
class TotalPriceSchema (Schema):
    items = fields.List(fields.Nested(ItemBillSchema), required=True)
    total_amount =  fields.Int(required=True)
    saved_amount = fields.Int(required=True)
    payable_amount =  fields.Int(required=True)
class InvoiceSchema(Schema):
    created_on  = fields.DateTime(required=True)
    customer = fields.Nested(CustomerSchema)
    user = fields.Str(required=True)
    items = fields.Nested(TotalPriceSchema, required=True)
    
    class Meta:
        unknown = EXCLUDE

    