from aiokafka import AIOKafkaProducer
import json
from base_settings import settings



class MessageProducer:
    @staticmethod
    async def send_one(message, topic):
        producer = AIOKafkaProducer(
            bootstrap_servers=settings.KAFKA_PORT)
        await producer.start()
        try:
            final_message =  json.dumps(message).encode('utf8')
            await producer.send_and_wait(topic=topic , value = final_message )
        finally:
            await producer.stop()
