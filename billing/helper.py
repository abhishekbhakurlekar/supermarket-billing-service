from billing.producer import MessageProducer
from base_settings import settings
import datetime
from config import KAFKA_TOPIC_DICT


class Invoice:
    @staticmethod
    async def generate(data):
        data["meta"] = {
            "created_at": datetime.datetime.now().isoformat(),
            "action": "generate_invoice",
        }
        await MessageProducer.send_one(data, KAFKA_TOPIC_DICT.get("GENERATE"))
