
from marshmallow import ValidationError
from billing.validator import GetitemSchema, BillSchema
from models import Item as ItemModel
from helpers import get_items, discount_helper
from fastapi import Request, HTTPException, status, APIRouter
from auth.helper import UserAuth
from billing.helper import Invoice
import asyncio
from error_helper import NormalizeErrors


billing_router = APIRouter(prefix="/billing", tags=['billing'])

@billing_router.post('/')
async def finalbill(request:Request):
    data = await request.json()
    token = request.headers.get('token') or ""
    data['user'] = await UserAuth.get_current_user(token)
    products ={}
    total_amount = 0
    discounted_amount = 0
    final_items = []
    item_names= []
    try:
        bill_serialized_data = BillSchema().load(data)
    except ValidationError as err:
        error_msg = NormalizeErrors('marshmallow', err.messages).normalize()
        raise HTTPException(status_code= status.HTTP_400_BAD_REQUEST, detail= error_msg)

    for item in bill_serialized_data.get('items',[]):
        name = item.pop('name')
        if products.get(name):
            products[name]["quantity"] += item.get('quantity') or 0
        else:
            products[name] = item
            item_names.append(name)
    items = await ItemModel.connection().distinct('name',{"name":{"$in":item_names}})
    diff = list(set(item_names).difference(set(items)))
    if diff:
        raise HTTPException(status_code= status.HTTP_400_BAD_REQUEST, detail= f"these items {diff} not found")
    all_items = await get_items(item_names)
    for item in all_items:
        quantity = products[item['name']]["quantity"]
        total_payable, actual_payable = discount_helper(item,quantity)
        final= { 'item': item['name'], "quantity" : quantity, "amount":actual_payable}
        final["quantity_unit"] = products[item['name']]["quantity_unit"]
        final_items.append(final)
        total_amount+=total_payable
        discounted_amount += actual_payable
    
    resp_dict = {
        "items" : final_items,
        "total_amount" : total_amount,
        "saved_amount": total_amount-discounted_amount,
        "payable_amount": discounted_amount

    }

    if bill_serialized_data.get('customer'):
        bill_serialized_data['items'] = resp_dict
        asyncio.create_task(Invoice.generate(bill_serialized_data))
    return resp_dict