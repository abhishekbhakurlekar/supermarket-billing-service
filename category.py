from marshmallow import ValidationError
from models import Category as CategoryModel
from fastapi import Request, HTTPException, status, APIRouter,Response
from pymongo.errors import DuplicateKeyError


category_router = APIRouter(prefix="/category", tags=['category'])



@category_router.get('/{category_name}')
async def get_category(category_name: str):
    
    category = await CategoryModel.connection().find_one({"name":category_name.lower()})
    if not category:
        raise HTTPException(status_code= status.HTTP_400_BAD_REQUEST, detail= f"sorry this category {category_name} not found")
    
    resp_dict = CategoryModel.insertSchema().dump(category)
    
        
    return resp_dict
    

@category_router.put('/{category_name}',status_code=status.HTTP_200_OK)
async def put_category(request: Request ,category_name: str):
    data = await request.json()
    
    category = await CategoryModel.connection().find_one({"name":category_name.lower()})
    if not category:
        raise HTTPException(status_code= status.HTTP_400_BAD_REQUEST, detail= f"sorry this category {category_name} not found")
    
    try:
        serialized_data = CategoryModel.insertSchema().load(data)
    except ValidationError as err:
        raise HTTPException(status_code= status.HTTP_400_BAD_REQUEST, detail= err.messages)
    
    await CategoryModel.connection().update_one({"name":category_name.lower()},{"$set": serialized_data})
    
    return {"success": True}



@category_router.post('/', status_code=status.HTTP_202_ACCEPTED)
async def post_category(request: Request):
    data = await request.json()
    try:
        serialized_data = CategoryModel.insertSchema().load(data)
        result = await CategoryModel.connection().insert_one(serialized_data) 
    except ValidationError as err:
        raise HTTPException(status_code= status.HTTP_400_BAD_REQUEST, detail= err.messages)
    except DuplicateKeyError:
        raise HTTPException(status_code= status.HTTP_400_BAD_REQUEST, detail= "Discount on these category already exists please try to update it")

    
    return {"success": True}
